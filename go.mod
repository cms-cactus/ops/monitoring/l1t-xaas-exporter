module l1t-xaas-exporter

go 1.15

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/valyala/fastjson v1.6.1
)
