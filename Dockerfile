FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7:tag-0.0.8 as runner

WORKDIR /

COPY ci_rpms/cactus-l1t-xaas-exporter-*.rpm /rpms/

RUN yum install -y /rpms/*.rpm && yum clean all

EXPOSE 2112

ENTRYPOINT ["/l1t-xaas-exporter"]