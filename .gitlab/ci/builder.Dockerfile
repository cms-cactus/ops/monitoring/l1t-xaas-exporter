# docker build -f builder.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/experimental/l1ce-golang/builder:$(date +%F) .

FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7:tag-0.0.8
LABEL maintainer="Cactus <cactus@cern.ch>"

ENV GO_VERSION=1.15.5

RUN yum install -y make gcc && \
    yum clean all

ENV PATH="$PATH:/usr/local/go/bin"
# golang
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz
