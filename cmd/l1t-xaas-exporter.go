package main

import (
	"flag"
	"fmt"
	"l1t-xaas-exporter/internal/xaas"
	"l1t-xaas-exporter/internal/xaas2prom"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func recordMetricsLoop() {
	go func() {
		failBackoff := 1
		for {
			err := recordMetrics()
			if err == nil {
				failBackoff = 1
			} else {
				failBackoff++
				if failBackoff > 10 {
					failBackoff = 10
				}
				log.Println("XaaS poll failed", err.Error())
				log.Println("Retrying in ", failBackoff, " seconds")
			}

			time.Sleep(time.Duration(failBackoff) * time.Second)
		}
	}()
}

func recordMetrics() error {
	pollsCounter.Inc()
	xaasData := make(map[string][]byte)
	l1tsData, err := xaas.FetchRaw("l1ts_cell")
	if err != nil {
		pollsFailCounter.Inc()
		return err
	}
	xaasData["l1ts_cell"] = l1tsData
	errors := xaas2prom.FromXaas(xaasData)
	for _, err := range errors {
		fmt.Println("error while processing xaas data", err)
	}
	return nil
}

var (
	pollsCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "l1t_xaas_exporter_xaas_polls_total",
		Help: "The total number of times retrieval of L1T XaaS data was attempted",
	})
	pollsFailCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "l1t_xaas_exporter_xaas_polls_fail_total",
		Help: "The total number of times retrieval of L1T XaaS data failed",
	})
)

func main() {
	recordMetricsLoop()
	xaas.Address = flag.String("xaas.address", "http://l1ts-xaas.cms:9945/urn:xdaq-application:lid=16", "address of the cell where to retrieve flashlists")
	flag.Parse()
	http.HandleFunc("/metrics", xaas2prom.SyncedDefaultHandleFunc)
	http.ListenAndServe(":2112", nil)
}
