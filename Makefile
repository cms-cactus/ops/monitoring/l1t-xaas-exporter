SHELL:=/bin/bash
.DEFAULT_GOAL := help

PROJECT_NAME=l1t-xaas-exporter
VERSION ?= $(shell git describe --always)
ARCH=amd64
RPM_NAME = cactus-${PROJECT_NAME}-${VERSION}-${RELEASE}.${ARCH}.rpm

src_files := $(shell find -name '*.go')

.PHONY: build
build: TAGS=production
build: build/l1t-xaas-exporter ## Builds project

.PHONY: rpm
rpm: ${RPM_NAME} ## Creates RPM package for the project

${RPM_NAME}: build 
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/bin/l1t-xaas-exporter rpmroot/usr/lib/systemd/system rpmroot/opt/cactus/etc/l1t-xaas-exporter rpmroot/opt/cactus/etc/default
	cp systemd/* rpmroot/usr/lib/systemd/system
	cp build/* rpmroot/opt/cactus/bin/l1t-xaas-exporter
	cp default/cactus-l1t-xaas-exporter.default rpmroot/opt/cactus/etc/default/cactus-l1t-xaas-exporter

	mkdir -p rpms
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "Flashlist exporter for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/l1t-xaas-exporter" \
	--provides cactus-${PROJECT_NAME} \
	.=/ && mv *.rpm ../rpms/

build/%: cmd/%.go $(src_files)
	go build -o $@ -tags=${TAGS} $<

.PHONY: test
test: TAGS=testing
test: build/l1t-xaas-exporter ## Build test app

.PHONY: clean
clean: ## Cleans up built stuff
	rm -rf build rpmroot rpms


.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'