package types

import (
	"encoding/json"
	"fmt"
)

// XaasResponse represents a json response from xaas
type XaasResponse struct {
	Table XaasTable `json:"table"`
}

// XaasTable is the table inside a XaasResponse
type XaasTable struct {
	Properties XaasTableProperties `json:"properties"`
	Rows       []interface{}       `json:"rows"`
}

// XaasTableProperties is part of XaasTable
type XaasTableProperties struct {
	Name       string `json:"Name"`
	LastUpdate string `json:"LastUpdate"`
	Version    string `json:"Version"`
}

// XaasSeverity is a severity comonly found in flashlists
type XaasSeverity string

// XaaS Alert Severities
const (
	AlertSeverityInfo    XaasSeverity = "INFO"
	AlertSeverityWarning              = "WARNING"
	AlertSeverityError                = "ERROR"
)

// UnmarshalJSON takes a string or int and converts it to XaasAlarmSeverity
func (b *XaasSeverity) UnmarshalJSON(bytes []byte) error {
	var raw interface{}
	if err := json.Unmarshal(bytes, &raw); err != nil {
		return err
	}
	if str, ok := raw.(string); ok {
		*b = XaasSeverity(str)
		return nil
	}
	if n, ok := raw.(float64); ok {
		if n == 0 {
			*b = AlertSeverityInfo
		} else if n == 1 {
			*b = AlertSeverityWarning
		} else {
			*b = AlertSeverityError
		}
		return nil
	}
	return fmt.Errorf("cannot infer XaaS alarm sensitivity '%s'", raw)
}
