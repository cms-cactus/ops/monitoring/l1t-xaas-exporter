// +build testing

package xaas

import (
	"io/ioutil"
)

var Address *string

func FetchRaw(flashlistName string) ([]byte, error) {
	return ioutil.ReadFile(*Address + "/" + flashlistName + ".json")
}
