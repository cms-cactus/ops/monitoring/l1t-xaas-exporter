# How to fetch example JSONs from P5

Make sure you do this during a run to get usable data.

```bash
for val in bmtf_cell calol1_cell calol2_cell cppf_cell diskInfo emtf_cell eventing-statistics l1ts_cell omtf_cell twinmux_cell ugmt_cell ugt_cell; do curl --fail "http://l1ts-xaas.cms:9945/urn:xdaq-application:lid=16/retrieveCollection?fmt=json&flash=urn:xdaq-flashlist:$val" > "$val.json"; done
```