// +build !testing

package xaas

import (
	"io/ioutil"
	"net/http"
)

// Address is the address of the cell providing flashlists
var Address *string

// FetchRaw retrieves flashlists
func FetchRaw(flashlistName string) ([]byte, error) {
	resp, err := http.Get(*Address + "/retrieveCollection?fmt=json&flash=urn:xdaq-flashlist:" + flashlistName)
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
