package xaas2prom

import (
	"l1t-xaas-exporter/types"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/valyala/fastjson"
)

type metricPair struct {
	config  Flashlist2MetricConfig
	counter *prometheus.CounterVec
	gauge   *prometheus.GaugeVec
}

var flashlistMetrics = []*metricPair{
	newFlashlistMetric(Flashlist2MetricConfig{"l1ts_cell", "l1t_xaas_subsystems", "Active XaaS subsystems", BooleanMode, func(value *fastjson.Value) ([]*prometheus.Labels, []float64) {
		subsystem := toString(value, "subsystem")
		application := toString(value, "name")
		uri := toString(value, "context")
		pid := toString(value, "PID")
		urn := toString(value, "urn")
		return []*prometheus.Labels{{
			"subsystem":   subsystem,
			"application": application,
			"uri":         uri,
			"pid":         pid,
			"urn":         urn,
		}}, nil
	}}),
	newFlashlistMetric(Flashlist2MetricConfig{"l1ts_cell", "l1t_xaas_alarms", "Active XaaS alarms", BooleanMode, func(value *fastjson.Value) ([]*prometheus.Labels, []float64) {
		subsystem := toString(value, "subsystem")
		application := toString(value, "name")
		labels := []*prometheus.Labels{}
		for _, alarm := range value.GetArray("Alarms", "rows") {
			var severity types.XaasSeverity
			severity.UnmarshalJSON(alarm.Get("severity").MarshalTo(nil))
			labels = append(labels, &prometheus.Labels{
				"subsystem":   subsystem,
				"application": application,
				"name":        toString(alarm, "name"),
				"message":     toString(alarm, "details"),
				"severity":    string(severity),
			})
		}
		return labels, nil
	}}),
	newFlashlistMetric(Flashlist2MetricConfig{"l1ts_cell", "l1t_xaas_rates", "Active XaaS rate warnings", GaugeMode, func(value *fastjson.Value) ([]*prometheus.Labels, []float64) {
		id := toString(value, "uniqueid")
		labels := []*prometheus.Labels{}
		values := []float64{}
		for _, rate := range value.GetArray("Rates", "rows") {
			var severity types.XaasSeverity
			severity.UnmarshalJSON(rate.Get("severity").MarshalTo(nil))
			labels = append(labels, &prometheus.Labels{
				"subsystem_id": id,
				"name":         toString(rate, "name"),
				"message":      toString(rate, "details"),
				"severity":     string(severity),
			})
			values = append(values, rate.Get("value").GetFloat64())
		}
		return labels, values
	}}),
}
