package xaas2prom

import (
	"net/http"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// RWMetricsLock is to be used when you want the /metrics collection endpoint to wait
// for your metrics alterations to be finished
var RWMetricsLock = &sync.Mutex{}

var promHandler = promhttp.HandlerFor(
	prometheus.DefaultGatherer,
	promhttp.HandlerOpts{
		EnableOpenMetrics: true,
	},
)

// SyncedDefaultHandleFunc is the default prometheus http handler, with
// syncronization added
var SyncedDefaultHandleFunc = func(w http.ResponseWriter, r *http.Request) {
	RWMetricsLock.Lock()
	defer RWMetricsLock.Unlock()
	promHandler.ServeHTTP(w, r)
}
