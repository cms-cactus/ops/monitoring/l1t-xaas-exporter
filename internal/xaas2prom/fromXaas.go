package xaas2prom

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/valyala/fastjson"
)

var p fastjson.Parser

func toString(v *fastjson.Value, keys string) string {
	b := v.GetStringBytes(keys)
	if b == nil {
		return ""
	}
	return string(b)
}

func newFlashlistMetric(config Flashlist2MetricConfig) *metricPair {
	return &metricPair{config, nil, nil}
}

// FromXaas updates prometheus counters with given XaaS data
func FromXaas(xaasData map[string][]byte) []error {
	errors := []error{}

	RWMetricsLock.Lock()
	defer RWMetricsLock.Unlock()

	for _, metric := range flashlistMetrics {
		if metric.config.Mode == CounterMode {
			continue
		}
		if metric.counter != nil {
			metric.counter.Reset()
		}
		if metric.gauge != nil {
			metric.gauge.Reset()
		}
	}

	for flashlistName, bytesJSON := range xaasData {
		root, err := p.ParseBytes(bytesJSON)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		rows := root.GetArray("table", "rows")
		if rows == nil {
			errors = append(errors, fmt.Errorf("invalid flashlist JSON from '%s'. table.rows is not an array", flashlistName))
			continue
		}
		for _, metric := range flashlistMetrics {
			if metric.config.FlastlistName != flashlistName {
				continue
			}
			for _, row := range rows {
				labels, values := metric.config.Converter(row)
				if labels == nil || len(labels) == 0 {
					// errors = append(errors, fmt.Errorf("converter for '%s'->'%s' did not return any data", flashlistName, metric.config.MetricName))
					continue
				}
				if metric.counter == nil && metric.gauge == nil {
					labelNames := []string{}
					for l := range *labels[0] {
						labelNames = append(labelNames, l)
					}
					if metric.config.Mode == GaugeMode {
						metric.gauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
							Name: metric.config.MetricName,
							Help: metric.config.Description,
						}, labelNames)
					} else {
						metric.counter = promauto.NewCounterVec(prometheus.CounterOpts{
							Name: metric.config.MetricName,
							Help: metric.config.Description,
						}, labelNames)
					}
				}
				if metric.config.Mode == GaugeMode {
					for i := range labels {
						metric.gauge.With(*labels[i]).Set(values[i])
					}
				} else if metric.config.Mode == BooleanMode {
					for i := range labels {
						metric.counter.With(*labels[i]).Inc()
					}
				} else if metric.config.Mode == CounterMode {
					for i := range labels {
						metric.counter.With(*labels[i]).Add(values[i])
					}
				} else {
					errors = append(errors, fmt.Errorf("config for '%s'->'%s' has unknown mode", flashlistName, metric.config.MetricName))
					continue
				}
			}
		}
	}
	return errors
}
