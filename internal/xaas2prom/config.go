package xaas2prom

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/valyala/fastjson"
)

// Flashlist2MetricConfig is used to turn (part of) a flashlist into a Prometheus metric
type Flashlist2MetricConfig struct {
	FlastlistName string
	MetricName    string
	Description   string
	Mode          MetricMode
	Converter     func(*fastjson.Value) ([]*prometheus.Labels, []float64)
}

// MetricMode describes what kind of metric a config represents
type MetricMode int

// known metric types
const (
	BooleanMode MetricMode = iota
	CounterMode
	GaugeMode
)
